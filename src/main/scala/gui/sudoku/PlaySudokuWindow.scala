package gui.sudoku

/**
 * Created by Piotr Wąchała
 */
import _root_.model.sudoku.Cell
import _root_.model.sudoku.tools.simple.generators.SudokuSimpleGeneratorWithoutBacktracking
import _root_.model.sudoku.tools.simple.solvers.SudokuSimpleSolverWithBacktracking

import scala.swing._
import scala.swing.event.ButtonClicked
import scala.util.Random

class PlaySudokuWindow extends MainFrame with GuiTrait{
  var sudoku = SudokuSimpleGeneratorWithoutBacktracking.generate();

  title = "Sudoku"
  preferredSize = windowSize
  maximumSize = windowSize
  resizable = false

  val doneButton = new Button("Done") {
    minimumSize = optionButtonsSize
    maximumSize = optionButtonsSize
  }

  val giveUpButton = new Button("Give up") {
    minimumSize = optionButtonsSize
    maximumSize = optionButtonsSize
  }

  val hintButton = new Button("Hint"){
    minimumSize = optionButtonsSize
    maximumSize = optionButtonsSize
  }

  val sudokuField = initSudokuField()

  val buttonsFlowPanel = new BoxPanel(Orientation.Vertical) {
    contents += doneButton
    contents += Swing.VStrut(10)
    contents += giveUpButton
    contents += Swing.VStrut(10)
    contents += hintButton
  }

  contents = new BoxPanel(Orientation.NoOrientation) {
    border = windowBorder
    contents += Swing.HStrut(10)
    contents += Swing.VStrut(10)
    contents += sudokuField
    contents += Swing.HStrut(10)
    contents += buttonsFlowPanel
    contents += Swing.VStrut(10)
    contents += Swing.HStrut(10)
  }

  listenTo(giveUpButton)
  listenTo(doneButton)
  listenTo(hintButton)

  reactions += {
    case ButtonClicked(`doneButton`) => {
      if(checkIfAllFieldsFilled == true)
        checkIfSolutionCorrect()
      else displayNotAllCellFilledMessage
    }

    case ButtonClicked(`giveUpButton`) =>
      if (Dialog.showConfirmation(contents.head, title = "Seriously?",
        message = "Do you really want to give up?") == Dialog.Result.Yes) {
        displayGiveUpMessage()
      }

    case ButtonClicked(`hintButton`) =>
      displayHint
  }

  def displayHint() = {
    SudokuSimpleSolverWithBacktracking.hint(sudoku) match{
      case (true, _) => {displayErrorInfo}
      case (false, Some(tuple))=> {
        sudoku.setLabel(Cell(tuple._1.row,tuple._1.column), tuple._2)
        textFieldsArray(tuple._1.row)(tuple._1.column).text = tuple._2.toString
        highlightCell(tuple._1.row, tuple._1.column,hintColor)
      }
      case (false, None) => {
        displayNoMoreHints
      }
    }
  }

  def displayWinningMessage() = {
    Dialog.showMessage(contents.head, title = "You win!",
      message = "Congratulations - you filled sudoku correctly!")

    val mainMenuWindow = new MainMenuWindow
    mainMenuWindow.visible = true
    close()
  }

  def displayLosingMessage() = {
    Dialog.showMessage(contents.head, title = "You lose!",
      message = "Your solution is not correct!")
  }

  def displayGiveUpMessage() = {
    Dialog.showMessage(contents.head, title = "You lose!",
      message = if(Random.nextInt(3) > 0) "HAHA, OWNED!" else "LOOSER!")

    val mainMenuWindow = new MainMenuWindow
    mainMenuWindow.visible = true
    close()
  }

  def displayNotAllCellFilledMessage(): Unit ={
    Dialog.showMessage(contents.head, title = "You're not done yet!",
      message = "You need to fill all cells!")
  }

  def checkIfSolutionCorrect(): Unit={
    SudokuSimpleSolverWithBacktracking.solve(sudoku) match{
      case (true, _ ) => displayLosingMessage
      case (false, Some(e)) => displayWinningMessage
    }
  }

  def displayErrorInfo() = {
    Dialog.showMessage(contents.head, title = "Wrong value found!",
      message = "One of the cells contains wrong value.")
  }

  def displayNoMoreHints() = {
    Dialog.showMessage(contents.head, title = "No more hints",
      message = "No more hints for you!")
  }

  def checkIfAllFieldsFilled(): Boolean = {
    var isFilled: Boolean = true;
    for (i <- (0 to 8)) {
      for (j <- (0 to 8))
        if (textFieldsArray(i)(j).text.length() < 1) {
          isFilled = false;
          highlightCell(i, j, errorColor)
        }
    }
    return isFilled
  }
}
