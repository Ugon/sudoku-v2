package model.sudoku

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author Wojciech Pachuta.
 */
@RunWith(classOf[JUnitRunner])
class SudokuTest extends FunSuite {

  test("equals test") {
    assert(new Sudoku(
      Array(
        Array(9, 3, 8, 2, 5, 6, 1, 4, 7),
        Array(4, 5, 6, 1, 8, 7, 9, 2, 3),
        Array(7, 2, 1, 9, 4, 3, 5, 8, 6),

        Array(3, 5, 7, 8, 9, 4, 2, 6, 1),
        Array(1, 4, 6, 7, 2, 5, 8, 3, 9),
        Array(8, 9, 2, 6, 3, 1, 7, 5, 4),

        Array(5, 7, 9, 3, 6, 8, 4, 1, 2),
        Array(2, 8, 3, 4, 1, 9, 6, 7, 5),
        Array(6, 1, 4, 5, 7, 2, 3, 9, 8)
      ))
      ==
      new Sudoku(
        Array(
          Array(9, 3, 8, 2, 5, 6, 1, 4, 7),
          Array(4, 5, 6, 1, 8, 7, 9, 2, 3),
          Array(7, 2, 1, 9, 4, 3, 5, 8, 6),

          Array(3, 5, 7, 8, 9, 4, 2, 6, 1),
          Array(1, 4, 6, 7, 2, 5, 8, 3, 9),
          Array(8, 9, 2, 6, 3, 1, 7, 5, 4),

          Array(5, 7, 9, 3, 6, 8, 4, 1, 2),
          Array(2, 8, 3, 4, 1, 9, 6, 7, 5),
          Array(6, 1, 4, 5, 7, 2, 3, 9, 8)
        )))
  }

}
