package model.sudoku

/**
 * @author Wojciech Pachuta.
 */
final class Cell private (val row: Int, val column: Int){

  private[sudoku] def peers: Set[Cell] = Cell.peersCache.getOrElseUpdate(this, Row(row).cells ++ Column(column).cells ++ Box(row/3,column/3) - this)

  private[sudoku] def dependentGroups: Set[Group] = Cell.dependentGroupsCache.getOrElseUpdate(this, Set(Row(row), Column(column), Box(row/3,column/3)))

  def canEqual(other: Any): Boolean = other.isInstanceOf[Cell]

  override def equals(other: Any): Boolean = other match {
    case that: Cell =>
      (that canEqual this) &&
        row == that.row &&
        column == that.column
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(row, column)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
  override def toString: String = "Cell(" + row + ", " + column +")"
}

object Cell {
  private val peersCache = collection.mutable.Map[Cell, Set[Cell]]()
  private val dependentGroupsCache = collection.mutable.Map[Cell, Set[Group]]()
  private val cellCache: Seq[Seq[Cell]] = (0 to 8).map(row => (0 to 8).map(column => new Cell(row, column)))
  def apply(row: Int, column: Int): Cell = cellCache(row)(column)
}