package model.sudoku.tools.simple.generators

import model.sudoku.tools.simple.{SudokuSimpleGenerator, SudokuSimpleRepresentation, SudokuSimpleSolver}
import model.sudoku.{Cell, Sudoku}

import scala.annotation.tailrec
import scala.util.Random

/**
 * @author Wojciech Pachuta.
 */
private[sudoku] abstract class AbstractSimpleSudokuGenerator(solver: SudokuSimpleSolver) extends SudokuSimpleGenerator{

  override def generateSudokuSimpleRepresentation(): SudokuSimpleRepresentation = {
    @tailrec def go(sudokuSimpleRepresentation: SudokuSimpleRepresentation, toRemove: List[Cell]): SudokuSimpleRepresentation = {
      toRemove match{
        case Nil => sudokuSimpleRepresentation
        case h :: t => {
          val cp = sudokuSimpleRepresentation.copy()
          cp.erase(h)
          if (solver.hasExactlyOneSolution(cp.toSudoku)) go(cp,t)
          else go(sudokuSimpleRepresentation,t)
        }
      }
    }

    Random.setSeed(System.currentTimeMillis())
    val result = go(base(), Random.shuffle(Sudoku.cells.toList))
    result
  }

  private def base(): SudokuSimpleRepresentation ={
    def go(sudokuRepresentation: SudokuSimpleRepresentation): Option[SudokuSimpleRepresentation] = {
      if(sudokuRepresentation.isSolved) Option(sudokuRepresentation)
      else{
        val (cell, positions) = sudokuRepresentation.cellWithLeastPossibilitiesMoreThanOne
        val iterator = positions.iterator
        var result: Option[SudokuSimpleRepresentation] = None
        var copy: SudokuSimpleRepresentation = null
        while(result.isEmpty && iterator.hasNext){
          copy = sudokuRepresentation.copy()
          if(!copy.assign(cell, iterator.next())) result = go(copy)
        }
        result
      }
    }

    val sudokuRepresentation = new SudokuSimpleRepresentation
    val iter = Random.shuffle(Sudoku.cells.toList).take(9).iterator
    (1 to 9).foreach(sudokuRepresentation.assign(iter.next(), _))
    go(sudokuRepresentation).get
  }
}


