package model.sudoku

/**
 * @author Wojciech Pachuta.
 */
sealed abstract class Group (val cells: Set[Cell]) extends Iterable[Cell]{
  override def iterator: Iterator[Cell] = cells.iterator
}

final class Row(cells: Set[Cell]) extends Group(cells)
private[sudoku] object Row{
  private val rowCache: Seq[Row] = (0 to 8).map(row => new Row((0 to 8).map(Cell(row,_)).toSet))
  def apply(row: Int) = rowCache(row)
}

final class Column(cells: Set[Cell]) extends Group(cells)
private[sudoku] object Column{
  private val columnCache: Seq[Column] = (0 to 8).map(column => new Column((0 to 8).map(Cell(_,column)).toSet))
  def apply(column: Int) = columnCache(column)
}

final class Box(cells: Set[Cell]) extends Group(cells)
private[sudoku] object Box{
  private val boxCache: Seq[Seq[Box]] = (0 to 8 by 3).map(x => (0 to 8 by 3).map(y => new Box((for(i <- 0 to 2; j <- 0 to 2) yield Cell(x + i, y + j)).toSet)))
  def apply(boxRow: Int, boxColumn: Int) = boxCache(boxRow)(boxColumn)
}

