package model.sudoku.tools.simple

import model.sudoku.Sudoku
import model.sudoku.tools.SudokuGenerator

/**
 * @author Wojciech Pachuta.
 */
private[sudoku] trait SudokuSimpleGenerator extends SudokuGenerator{

  override def generate(): Sudoku = generateSudokuSimpleRepresentation().toSudoku

  private[sudoku] def generateSudokuSimpleRepresentation(): SudokuSimpleRepresentation

}
