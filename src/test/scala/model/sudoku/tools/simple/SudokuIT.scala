package model.sudoku.tools.simple

import model.sudoku.tools.simple.generators.SudokuSimpleGeneratorWithoutBacktracking
import model.sudoku.tools.simple.solvers.SudokuSimpleSolverWithBacktracking
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author Wojciech Pachuta.
 */
@RunWith(classOf[JUnitRunner])
class SudokuIT extends FunSuite{
  val generator = SudokuSimpleGeneratorWithoutBacktracking
  val solver = SudokuSimpleSolverWithBacktracking

  test("should hints solve generated sudoku") {
    val sudoku = generator.generate()
    
    //when
    while(!solver.isSolved(sudoku)){
      assert(!solver.isContrary(sudoku))
      val hint = solver.hint(sudoku)
      assert(!hint._1)
      sudoku.setLabel(hint._2.get._1, hint._2.get._2)
    }

    //then
    assert(solver.isSolved(sudoku))
  }

}
