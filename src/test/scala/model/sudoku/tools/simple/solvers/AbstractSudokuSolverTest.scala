package model.sudoku.tools.simple.solvers

import model.sudoku.tools.SudokuSolver
import model.sudoku.{Cell, Sudoku}
import org.scalatest.FunSuite

/**
 * @author Wojciech Pachuta.
 */

abstract class AbstractSudokuSolverTest(val sudokuSolver: SudokuSolver) extends FunSuite {

  test("should solve() solve easy puzzle") {
    //given
    val easySudoku = new Sudoku(
      Array(
        Array(0, 0, 8, 2, 0, 0, 1, 4, 7),
        Array(4, 6, 0, 0, 0, 0, 9, 2, 0),
        Array(7, 0, 0, 9, 0, 0, 0, 8, 0),

        Array(0, 0, 7, 0, 9, 0, 0, 6, 1),
        Array(0, 4, 0, 7, 0, 5, 0, 3, 0),
        Array(8, 9, 0, 0, 3, 0, 7, 0, 0),

        Array(0, 7, 0, 0, 0, 8, 0, 0, 2),
        Array(0, 8, 3, 0, 0, 0, 0, 7, 5),
        Array(6, 1, 4, 0, 0, 2, 3, 0, 0)
      ))

    val expectedResult = new Sudoku(
      Array(
        Array(9, 3, 8, 2, 5, 6, 1, 4, 7),
        Array(4, 6, 5, 1, 8, 7, 9, 2, 3),
        Array(7, 2, 1, 9, 4, 3, 5, 8, 6),

        Array(3, 5, 7, 8, 9, 4, 2, 6, 1),
        Array(1, 4, 6, 7, 2, 5, 8, 3, 9),
        Array(8, 9, 2, 6, 3, 1, 7, 5, 4),

        Array(5, 7, 9, 3, 6, 8, 4, 1, 2),
        Array(2, 8, 3, 4, 1, 9, 6, 7, 5),
        Array(6, 1, 4, 5, 7, 2, 3, 9, 8)
      ))

    //when
    val result = sudokuSolver.solve(easySudoku)

    //then
    assert(!result._1)
    assert(result._2.isDefined)
    assert(result._2.get == expectedResult)
  }

  test("should solve() solve medium puzzle") {
    //given
    val mediumSudoku = new Sudoku(
      Array(
        Array(0, 0, 0, 0, 2, 0, 9, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 4, 2),
        Array(0, 1, 9, 0, 0, 8, 0, 0, 6),

        Array(0, 8, 7, 4, 5, 0, 0, 0, 0),
        Array(0, 5, 0, 0, 0, 0, 0, 8, 0),
        Array(0, 0, 0, 0, 9, 2, 7, 3, 0),

        Array(4, 0, 0, 5, 0, 0, 6, 1, 0),
        Array(1, 6, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 3, 0, 6, 0, 0, 0, 0)
      ))

    val expectedResult = new Sudoku(
      Array(
        Array(8, 3, 4, 6, 2, 5, 9, 7, 1),
        Array(5, 7, 6, 9, 1, 3, 8, 4, 2),
        Array(2, 1, 9, 7, 4, 8, 3, 5, 6),

        Array(3, 8, 7, 4, 5, 1, 2, 6, 9),
        Array(9, 5, 2, 3, 7, 6, 1, 8, 4),
        Array(6, 4, 1, 8, 9, 2, 7, 3, 5),

        Array(4, 2, 8, 5, 3, 9, 6, 1, 7),
        Array(1, 6, 5, 2, 8, 7, 4, 9, 3),
        Array(7, 9, 3, 1, 6, 4, 5, 2, 8)
      ))

    //when
    val result = sudokuSolver.solve(mediumSudoku)

    //then
    assert(!result._1)
    assert(result._2.isDefined)
    assert(result._2.get == expectedResult)
  }

  test("should solve() determine when puzzle is contrary when its obvious") {
    //given
    val contrarySudoku = new Sudoku(
      Array(
        Array(1, 1, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0)
      ))

    //when
    val result = sudokuSolver.solve(contrarySudoku)

    //then
    assert(result._1)
  }

  test("should solve() determine when puzzle is contrary when its not obvious") {
    //given
    val contrarySudoku = new Sudoku(
      Array(
        Array(3, 0, 0, 0, 2, 0, 9, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 4, 2),
        Array(0, 1, 9, 0, 0, 8, 0, 0, 6),

        Array(0, 8, 7, 4, 5, 0, 0, 0, 0),
        Array(0, 5, 0, 0, 0, 0, 0, 8, 0),
        Array(0, 0, 0, 0, 9, 2, 7, 3, 0),

        Array(4, 0, 0, 5, 0, 0, 6, 1, 0),
        Array(1, 6, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 3, 0, 6, 0, 0, 0, 0)
      ))

    //when
    val result = sudokuSolver.solve(contrarySudoku)

    //then
    assert(result._1)
  }

  test("should hasExactlyOneSolution() return true when expected") {
    //given
    val easySudoku = new Sudoku(
      Array(
        Array(0, 0, 8, 2, 0, 0, 1, 4, 7),
        Array(4, 6, 0, 0, 0, 0, 9, 2, 0),
        Array(7, 0, 0, 9, 0, 0, 0, 8, 0),

        Array(0, 0, 7, 0, 9, 0, 0, 6, 1),
        Array(0, 4, 0, 7, 0, 5, 0, 3, 0),
        Array(8, 9, 0, 0, 3, 0, 7, 0, 0),

        Array(0, 7, 0, 0, 0, 8, 0, 0, 2),
        Array(0, 8, 3, 0, 0, 0, 0, 7, 5),
        Array(6, 1, 4, 0, 0, 2, 3, 0, 0)
      ))

    //when
    val result = sudokuSolver.hasExactlyOneSolution(easySudoku)

    //then
    assert(result)
  }


  test("should hasExactlyOneSolution() return false when expected") {
    //given
    val easySudoku = new Sudoku(
      Array(
        Array(9, 3, 8, 0, 5, 6, 0, 4, 7),
        Array(4, 6, 5, 0, 8, 7, 9, 0, 3),
        Array(7, 0, 0, 9, 4, 3, 5, 8, 6),

        Array(3, 5, 7, 8, 9, 4, 0, 6, 0),
        Array(0, 4, 6, 7, 0, 5, 8, 3, 9),
        Array(8, 9, 0, 6, 3, 0, 7, 5, 4),

        Array(5, 7, 9, 3, 6, 8, 4, 0, 0),
        Array(0, 8, 3, 4, 0, 9, 6, 7, 5),
        Array(6, 0, 4, 5, 7, 0, 3, 9, 8)
      ))

    //when
    val result = sudokuSolver.hasExactlyOneSolution(easySudoku)

    //then
    assert(!result)
  }


  test("should hasExactlyOneSolution() return false on empty puzzle") {
    //given
    val emptySudoku = new Sudoku(
      Array(
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0)
      ))

    //when
    val result = sudokuSolver.hasExactlyOneSolution(emptySudoku)

    //then
    assert(!result)
  }

  test("should hasExactlyOneSolution() determine when puzzle is contrary when its obvious") {
    //given
    val contrarySudoku = new Sudoku(
      Array(
        Array(1, 1, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0)
      ))

    //when
    val result = sudokuSolver.solve(contrarySudoku)

    //then
    assert(result._1)
  }

  test("should hasExactlyOneSolution() determine when puzzle is contrary when its not obvious") {
    //given
    val contrarySudoku = new Sudoku(
      Array(
        Array(3, 0, 0, 0, 2, 0, 9, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 4, 2),
        Array(0, 1, 9, 0, 0, 8, 0, 0, 6),

        Array(0, 8, 7, 4, 5, 0, 0, 0, 0),
        Array(0, 5, 0, 0, 0, 0, 0, 8, 0),
        Array(0, 0, 0, 0, 9, 2, 7, 3, 0),

        Array(4, 0, 0, 5, 0, 0, 6, 1, 0),
        Array(1, 6, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 3, 0, 6, 0, 0, 0, 0)
      ))

    //when
    val result = sudokuSolver.solve(contrarySudoku)

    //then
    assert(result._1)
  }

  test("should hasExactlyOneSolution() return true on trivial puzzle") {
    //given
    val trivialSudoku = new Sudoku(
      Array(
        Array(0, 3, 4, 6, 2, 5, 9, 7, 1),
        Array(5, 7, 6, 9, 1, 3, 8, 4, 2),
        Array(2, 1, 9, 7, 4, 8, 3, 5, 6),

        Array(3, 8, 7, 4, 5, 1, 2, 6, 9),
        Array(9, 5, 2, 3, 7, 6, 1, 8, 4),
        Array(6, 4, 1, 8, 9, 2, 7, 3, 5),

        Array(4, 2, 8, 5, 3, 9, 6, 1, 7),
        Array(1, 6, 5, 2, 8, 7, 4, 9, 3),
        Array(7, 9, 3, 1, 6, 4, 5, 2, 8)
      ))

    //when
    val result = sudokuSolver.hasExactlyOneSolution(trivialSudoku)

    //then
    assert(result)
  }

  test("should hint() give not contrary hint") {
    //given
    val easySudoku = new Sudoku(
      Array(
        Array(0, 0, 8, 2, 0, 0, 1, 4, 7),
        Array(4, 6, 0, 0, 0, 0, 9, 2, 0),
        Array(7, 0, 0, 9, 0, 0, 0, 8, 0),

        Array(0, 0, 7, 0, 9, 0, 0, 6, 1),
        Array(0, 4, 0, 7, 0, 5, 0, 3, 0),
        Array(8, 9, 0, 0, 3, 0, 7, 0, 0),

        Array(0, 7, 0, 0, 0, 8, 0, 0, 2),
        Array(0, 8, 3, 0, 0, 0, 0, 7, 5),
        Array(6, 1, 4, 0, 0, 2, 3, 0, 0)
      ))


    //when
    val result: (Boolean, Option[(Cell, Int)]) = sudokuSolver.hint(easySudoku)

    //then
    assert(!result._1)
    assert(result._2.isDefined)
    assert(result._2.get._1.peers.forall(cell => easySudoku.getLabel(cell).isEmpty || easySudoku.getLabel(cell).get != result._2.get._2))

  }

  test("should hint() return true when contrary") {
    //given
    val contrarySudoku = new Sudoku(
      Array(
        Array(1, 1, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0)
      ))


    //when
    val result: (Boolean, Option[(Cell, Int)]) = sudokuSolver.hint(contrarySudoku)

    //then
    assert(result._1)
  }

  test("should hints solve sudoku") {
    val emptySudoku = new Sudoku(
      Array(
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0)
      ))


    //when
    while(!sudokuSolver.isSolved(emptySudoku)){
      assert(!sudokuSolver.isContrary(emptySudoku))
      val hint = sudokuSolver.hint(emptySudoku)
      assert(!hint._1)
      emptySudoku.setLabel(hint._2.get._1, hint._2.get._2)
    }

    //then
    assert(sudokuSolver.isSolved(emptySudoku))
  }

}
