package model.sudoku.tools.simple.solvers

import model.sudoku.Sudoku
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * @author Wojciech Pachuta.
 */
@RunWith(classOf[JUnitRunner])
class SudokuSimpleSolverWithBacktrackingTest extends AbstractSudokuSolverTest(SudokuSimpleSolverWithBacktracking){

  test("should solver solve hard puzzle") {
    //given
    val hardSudoku1 = new Sudoku(
      Array(
        Array(5, 0, 0, 0, 4, 0, 0, 0, 0),
        Array(0, 3, 0, 1, 0, 0, 6, 0, 0),
        Array(1, 0, 6, 0, 5, 8, 0, 0, 0),

        Array(0, 0, 9, 6, 0, 0, 5, 3, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 2, 7, 0, 0, 4, 8, 0, 0),

        Array(0, 0, 0, 2, 6, 0, 7, 0, 1),
        Array(0, 0, 4, 0, 0, 1, 0, 6, 0),
        Array(0, 0, 0, 0, 3, 0, 0, 0, 9)
      ))

    val expectedResult = new Sudoku(
      Array(
        Array(5, 9, 2, 3, 4, 6, 1, 7, 8),
        Array(7, 3, 8, 1, 2, 9, 6, 5, 4),
        Array(1, 4, 6, 7, 5, 8, 9, 2, 3),

        Array(4, 1, 9, 6, 8, 2, 5, 3, 7),
        Array(8, 6, 5, 9, 7, 3, 4, 1, 2),
        Array(3, 2, 7, 5, 1, 4, 8, 9, 6),

        Array(9, 8, 3, 2, 6, 5, 7, 4, 1),
        Array(2, 7, 4, 8, 9, 1, 3, 6, 5),
        Array(6, 5, 1, 4, 3, 7, 2, 8, 9)
      ))

    //when
    val result = sudokuSolver.solve(hardSudoku1)

    //then
    assert(!result._1)
    assert(result._2.isDefined)
    assert(result._2.get == expectedResult)
  }


  test("should solve() solve empty puzzle") {
    //given
    val emptySudoku = new Sudoku(
      Array(
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0)
      ))

    //when
    val result = sudokuSolver.solve(emptySudoku)

    //then
    assert(!result._1)
    assert(result._2.isDefined)
  }
}
