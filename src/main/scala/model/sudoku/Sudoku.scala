package model.sudoku

import scala.collection.mutable

/**
 * @author Wojciech Pachuta.
 */
class Sudoku (grid: Array[Array[Int]]){
  private val assignments: mutable.Map[Cell, Option[Int]] = mutable.Map() ++ Sudoku.cells.map(_ -> None).toMap
  for(i <- 0 to 8; j<- 0 to 8 if grid(i)(j) != 0) assignments.put(Cell(i,j), Some(grid(i)(j)))

  def getLabel(cell: Cell): Option[Int] = assignments(cell)

  def setLabel(cell: Cell, label: Int): Boolean = {
    assignments.put(cell, Some(label))
    if (cell.peers.exists(assignments(_).getOrElse(-1) == label)) true
    else false
  }

  def eraseLabel(cell: Cell): Unit = assignments.put(cell, None)

  def filledCells: Map[Cell, Int] = assignments.filter(_._2.isDefined).map({case (k, Some(v)) => (k, v)}).toMap

  def isSolved: Boolean = filledCells.size == 81

  def canEqual(other: Any): Boolean = other.isInstanceOf[Sudoku]

  override def equals(other: Any): Boolean = other match {
    case that: Sudoku =>
      (that canEqual this) &&
        assignments == that.assignments
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(assignments)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  def this() = {
    this(Array.ofDim[Int](9,9))
  }

}

object Sudoku{
  lazy val cells: Set[Cell] = (for(i <- 0 to 8; j <- 0 to 8) yield Cell(i,j)).toSet
  lazy val groups: Set[Group] = (rows ++ columns ++ boxes.flatten).toSet
  private[sudoku] lazy val rows: Seq[Row] = (0 to 8).map(Row(_))
  private[sudoku] lazy val columns: Seq[Column] = (0 to 8).map(Column(_))
  private[sudoku] lazy val boxes: Seq[Seq[Box]] = (0 to 2).map(x => (0 to 2).map(y => Box(x,y)))
}
