package model.sudoku.tools.simple.solvers

import model.sudoku.Cell
import model.sudoku.tools.simple.{SudokuSimpleRepresentation, SudokuSimpleSolver}

import scala.annotation.tailrec

/**
 * @author Wojciech Pachuta.
 */
object SudokuSimpleSolverWithBacktracking extends SudokuSimpleSolver {

  //(contrary, solution)
  private[sudoku] override def solve(sudokuSimpleRepresentation: SudokuSimpleRepresentation): (Boolean, Option[SudokuSimpleRepresentation]) = {
    def trySolve(original: SudokuSimpleRepresentation): Option[SudokuSimpleRepresentation] = {
      if (original.isSolved) Some(original)
      else {
        val cellPos = original.cellWithLeastPossibilitiesMoreThanOne
        first(original, cellPos._1, cellPos._2)
      }
    }

    @tailrec def first(original: SudokuSimpleRepresentation, cell: Cell, possibilities: Set[Int]): Option[SudokuSimpleRepresentation] = {
      if (possibilities.isEmpty) None
      else checkPossibility(original, cell, possibilities.head) match {
        case s@Some(some) => s
        case None => first(original, cell, possibilities.tail)
      }
    }

    def checkPossibility(original: SudokuSimpleRepresentation, cell: Cell, label: Int): Option[SudokuSimpleRepresentation] = {
      val copy = original.copy()
      if (copy.assign(cell, label)) None
      else trySolve(copy)
    }

    trySolve(sudokuSimpleRepresentation) match {
      case s @ Some(some) => (false, s)
      case None => (true, None)
    }
  }

  private[sudoku] override def hasExactlyOneSolution(sudokuSimpleRepresentation: SudokuSimpleRepresentation): Boolean = {
    def numberOfSolutions(original: SudokuSimpleRepresentation, acc: Int): Int = {
      if(acc == 2) 2
      else if (original.isSolved) acc + 1
      else {
        val cellPos = original.cellWithLeastPossibilitiesMoreThanOne
        count(original, cellPos._1, cellPos._2, acc)
      }
    }

    @tailrec def count(original: SudokuSimpleRepresentation, cell: Cell, possibilities: Set[Int], acc: Int): Int = {
      if (acc == 2) 2
      else if (possibilities.isEmpty) acc
      else count(original, cell, possibilities.tail, checkPossibility(original, cell, possibilities.head, acc))
    }

    def checkPossibility(original: SudokuSimpleRepresentation, cell: Cell, label: Int, acc: Int): Int = {
      val copy = original.copy()
      if (copy.assign(cell, label)) acc
      else numberOfSolutions(copy, acc)
    }

    numberOfSolutions(sudokuSimpleRepresentation, 0) == 1
  }

}