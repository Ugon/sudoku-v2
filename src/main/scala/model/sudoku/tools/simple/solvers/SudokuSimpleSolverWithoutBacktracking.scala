package model.sudoku.tools.simple.solvers

import model.sudoku.tools.simple.{SudokuSimpleRepresentation, SudokuSimpleSolver}

/**
 * @author Wojciech Pachuta.
 */
object SudokuSimpleSolverWithoutBacktracking extends SudokuSimpleSolver{

  //(contrary, solution)
  private[sudoku] override def solve(sudokuSimpleRepresentation: SudokuSimpleRepresentation): (Boolean, Option[SudokuSimpleRepresentation]) = {
    if (sudokuSimpleRepresentation.isSolved) (false, Some(sudokuSimpleRepresentation))
    else (false, None)
  }

  private[sudoku] override def hasExactlyOneSolution(sudokuSimpleRepresentation: SudokuSimpleRepresentation): Boolean = solve(sudokuSimpleRepresentation) match {
      case (false, Some(_)) => true
      case _ => false
  }

}
