package model.sudoku.tools.simple.generators

import model.sudoku.tools.simple.solvers.SudokuSimpleSolverWithBacktracking

/**
 * @author Wojciech Pachuta.
 */
object SudokuSimpleGeneratorWithBacktracking extends AbstractSimpleSudokuGenerator(SudokuSimpleSolverWithBacktracking)
