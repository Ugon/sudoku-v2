name := "sudoku"

version := "0.1.0"

scalaVersion := "2.11.4"

libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test",
  "junit" % "junit" % "4.10" % "test",
  "org.mockito" % "mockito-core" % "1.10.8",
  "org.mockito" % "mockito-all" % "1.10.8",
  "org.scala-lang.modules" %% "scala-swing" % "1.0.1"
)

lazy val root = project.in(file("."))