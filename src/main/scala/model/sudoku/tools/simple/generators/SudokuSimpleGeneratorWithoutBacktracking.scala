package model.sudoku.tools.simple.generators

import model.sudoku.tools.simple.solvers.SudokuSimpleSolverWithoutBacktracking

/**
 * @author Wojciech Pachuta.
 */
object SudokuSimpleGeneratorWithoutBacktracking extends AbstractSimpleSudokuGenerator(SudokuSimpleSolverWithoutBacktracking)

