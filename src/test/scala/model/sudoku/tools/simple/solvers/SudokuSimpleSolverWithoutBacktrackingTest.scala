package model.sudoku.tools.simple.solvers

import model.sudoku.Sudoku
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * @author Wojciech Pachuta.
 */
@RunWith(classOf[JUnitRunner])
class SudokuSimpleSolverWithoutBacktrackingTest extends AbstractSudokuSolverTest(SudokuSimpleSolverWithoutBacktracking){

  test("should solver not solve hard puzzle") {
    //given
    val hardSudoku1 = new Sudoku(
      Array(
        Array(5, 0, 0, 0, 4, 0, 0, 0, 0),
        Array(0, 3, 0, 1, 0, 0, 6, 0, 0),
        Array(1, 0, 6, 0, 5, 8, 0, 0, 0),

        Array(0, 0, 9, 6, 0, 0, 5, 3, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 2, 7, 0, 0, 4, 8, 0, 0),

        Array(0, 0, 0, 2, 6, 0, 7, 0, 1),
        Array(0, 0, 4, 0, 0, 1, 0, 6, 0),
        Array(0, 0, 0, 0, 3, 0, 0, 0, 9)
      ))

    //when
    val result = sudokuSolver.solve(hardSudoku1)


    //then
    assert(!result._1)
    assert(!result._2.isDefined)
  }


  test("should solve() not solve empty puzzle") {
    //given
    val emptySudoku = new Sudoku(
      Array(
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),

        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0),
        Array(0, 0, 0, 0, 0, 0, 0, 0, 0)
      ))

    //when
    val result = sudokuSolver.solve(emptySudoku)

    //then
    assert(!result._1)
    assert(!result._2.isDefined)
  }

}
