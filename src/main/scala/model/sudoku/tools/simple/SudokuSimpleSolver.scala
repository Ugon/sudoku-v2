package model.sudoku.tools.simple

import model.sudoku.{Cell, Sudoku}
import model.sudoku.tools.SudokuSolver
import model.sudoku.tools.simple.SudokuSimpleRepresentation

/**
 * @author Wojciech Pachuta.
 */
private[model] trait SudokuSimpleSolver extends SudokuSolver{

  override def solve(sudoku: Sudoku): (Boolean, Option[Sudoku]) = {
    val sudokuSimpleRepresentation = new SudokuSimpleRepresentation
    if (sudokuSimpleRepresentation.fillIn(sudoku)) (true, None)
    else solve(sudokuSimpleRepresentation) match{
      case (true, _) => (true, None)
      case (false, Some(s)) => (false, Some(s.toSudoku))
      case (false, None) => (false, None)
    }
  }

  override def hasExactlyOneSolution(sudoku: Sudoku): Boolean = {
    val sudokuSimpleRepresentation = new SudokuSimpleRepresentation
    !sudokuSimpleRepresentation.fillIn(sudoku) && hasExactlyOneSolution(sudokuSimpleRepresentation)
  }

  override def hint(sudoku: Sudoku): (Boolean, Option[(Cell, Int)]) = {
    if(sudoku.isSolved) (false, None)
    else {
      val sudokuSimpleRepresentation = new SudokuSimpleRepresentation
      if (sudokuSimpleRepresentation.fillInNoPropagation(sudoku)) (true, None)
      else {
        def isProperHint(cell: Cell, label: Int): Boolean = {
          val copy = sudokuSimpleRepresentation.copy()
          !copy.assign(cell, label) && !solve(copy)._1
        }
        val (cell, hints) = sudokuSimpleRepresentation.cellsWithLeastPossibilities
          .filter(e => sudoku.getLabel(e._1).isEmpty).head
        if(hints.find(isProperHint(cell, _)).isEmpty) (true, None)
        else (false, Option(cell, hints.find(isProperHint(cell, _)).get))
      }
    }
  }

  private[sudoku] def solve(sudokuSimpleRepresentation: SudokuSimpleRepresentation): (Boolean, Option[SudokuSimpleRepresentation])

  private[sudoku] def hasExactlyOneSolution(sudokuSimpleRepresentation: SudokuSimpleRepresentation): Boolean

  override def isContrary(sudoku: Sudoku): Boolean = solve(sudoku)._1

}
