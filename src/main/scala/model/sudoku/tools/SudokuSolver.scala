package model.sudoku.tools

import model.sudoku.{Cell, Sudoku, Group}

/**
 * @author Wojciech Pachuta.
 */
trait SudokuSolver {

  //(contrary, solution)
  def solve(sudoku:Sudoku): (Boolean, Option[Sudoku])

  private[sudoku] def hasExactlyOneSolution(sudoku:Sudoku): Boolean

  //(contrary, Option(cell, label))
  def hint(sudoku: Sudoku): (Boolean, Option[(Cell, Int)])

  def isContrary(sudoku: Sudoku): Boolean

  def isSolved(sudoku: Sudoku): Boolean = {
    assert(!isContrary(sudoku))
    sudoku.filledCells.size == 81
  }

}
