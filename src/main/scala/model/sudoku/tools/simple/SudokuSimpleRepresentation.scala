package model.sudoku.tools.simple

import model.sudoku.{Cell, Sudoku}

import scala.collection.mutable

/**
 * @author Wojciech Pachuta.
 */
private[sudoku] class SudokuSimpleRepresentation{
  private val possibilities: Map[Cell, mutable.Set[Int]] = (for(i <- 0 to 8; j <- 0 to 8) yield Cell(i,j)).map(_ -> (1 to 9).to[mutable.Set]).toMap

  def fillIn (sudoku: Sudoku): Boolean = sudoku.filledCells.exists(entry => assign(entry._1, entry._2))

  def assign(cell: Cell, label: Int): Boolean = {
    if(!possibilities(cell).contains(label)) true
    else possibilities(cell).filter(_ != label).exists(eliminate(cell, _))
  }

  private def eliminate(cell: Cell, label: Int): Boolean = {
    if (!possibilities(cell).contains(label)) false
    else {
      possibilities(cell) -= label
      if (possibilities(cell).size == 0) true
      else if (possibilities(cell).size == 1 && cell.peers.exists(eliminate(_, possibilities(cell).head))) true
      else {
        cell.dependentGroups.exists(_.filter(possibilities(_).contains(label)) match {
          case a if a.size == 0 => true
          case a if a.size == 1 => assign(a.head, label)
          case _ => false
        })
      }
    }
  }

  def erase(cell: Cell): Unit = possibilities(cell) ++= (1 to 9)

  def cellsWithLeastPossibilities: List[(Cell, Set[Int])] = possibilities.toList.sortBy(_._2.size).map(tuple => (tuple._1, tuple._2.toSet))

  def cellWithLeastPossibilitiesMoreThanOne: (Cell, Set[Int]) = cellsWithLeastPossibilities.find(_._2.size > 1).get

  def isSolved: Boolean = Sudoku.cells.forall(possibilities(_).size == 1)

  def solution: Option[Sudoku] = {
    isSolved match{
      case false => None
      case true =>{
        val solution = new Sudoku
        Sudoku.cells.foreach(cell => solution.setLabel(cell, possibilities(cell).head))
        Some(solution)
      }
    }
  }

  def copy(): SudokuSimpleRepresentation = {
    val copy = new SudokuSimpleRepresentation
    Sudoku.cells.foreach(cell => copy.possibilities(cell).retain(this.possibilities(cell).contains))
    copy
  }

  def toSudoku: Sudoku = {
    val sudoku = new Sudoku
    possibilities.filter(_._2.size == 1).foreach(tuple => sudoku.setLabel(tuple._1,tuple._2.head))
    sudoku
  }

  private def assignNoPropagation(cell: Cell, label: Int): Boolean = {
    if(!possibilities(cell).contains(label)) true
    else {
      possibilities(cell).retain(_ == label)
      cell.peers.foreach(possibilities(_) -= label)
      cell.peers.exists(possibilities(_)isEmpty)
    }
  }

  def fillInNoPropagation (sudoku: Sudoku): Boolean = sudoku.filledCells.exists(entry => assignNoPropagation(entry._1, entry._2))
}