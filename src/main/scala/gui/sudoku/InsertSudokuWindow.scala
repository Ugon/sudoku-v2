package gui.sudoku

/**
 * Created by Piotr Wąchała
 */

import _root_.model.sudoku.tools.simple.solvers.SudokuSimpleSolverWithBacktracking
import _root_.model.sudoku.{Cell, Sudoku}

import scala.swing._
import scala.swing.event.ButtonClicked

class InsertSudokuWindow extends MainFrame with GuiTrait{
  var sudoku = new Sudoku

  val backButton = new Button("Back"){
    minimumSize = optionButtonsSize
    maximumSize = optionButtonsSize
  }

  val solveButton = new Button("Solve"){
    minimumSize = optionButtonsSize
    maximumSize = optionButtonsSize
  }

  title = "Sudoku"
  minimumSize = windowSize
  maximumSize = windowSize
  preferredSize = windowSize
  resizable = false

  val sudokuField = initSudokuField()

  val buttonsPanel = new BoxPanel(Orientation.Vertical){
    contents +=backButton
    contents += Swing.VStrut(10)
    contents +=solveButton
  }

  contents = new BoxPanel(Orientation.NoOrientation) {
    border = windowBorder
    contents += Swing.HStrut(10)
    contents += Swing.VStrut(10)
    contents += sudokuField
    contents += Swing.HStrut(10)
    contents += buttonsPanel
    contents += Swing.VStrut(10)
    contents += Swing.HStrut(10)
  }

  listenTo(backButton)
  listenTo(solveButton)

  reactions +={
    case ButtonClicked(`backButton`) =>
      if (Dialog.showConfirmation(contents.head,
        "Are you sure you want quit?") == Dialog.Result.Yes) {
        val mainMenuWindow = new MainMenuWindow
        mainMenuWindow.visible = true
        close()
      }

    case ButtonClicked(`solveButton`) =>
      SudokuSimpleSolverWithBacktracking.solve(sudoku) match{
        case (true, _ ) => displayNoSolutionMessage
        case (false, Some(e)) => {displayThereIsSolution; showSolution(e)}
      }
  }

  def showSolution(solution: Sudoku): Unit ={
    for (j <- (0 to 8)) {
      var starti = 3*(j%3)
      var startj = 0;
      if(j<3) startj = 0;
      else if(j<6) startj = 3;
      else startj = 6;
      for( k <- (0 to 2))
        for( l <- (0 to 2)) {
          var newX = startj + k;
          var newY = starti + l;
          textFieldsArray(newX)(newY).text = solution.getLabel(Cell(newX, newY)).get.toString
        }
    }
  }

  def displayNoSolutionMessage: Unit ={
    Dialog.showMessage(contents.head, title = "No solution",
      message = "This sudoku has no solution!")
  }

  def displayThereIsSolution: Unit = {
    Dialog.showMessage(contents.head, title = "Solution found",
      message = "This sudoku has a solution!")
  }
}
