package gui.sudoku

/**
 * Created by Piotr Wąchała
 */
import javax.swing.ImageIcon

import scala.swing._
import scala.swing.event.ButtonClicked

class MainMenuWindow extends MainFrame {
  title = "Sudoku"
  preferredSize_=(new Dimension(350, 350))
  resizable = false

  val playButton = new Button("Play")
  val solveButton = new Button("Solve")

  val label = new Label {
    icon = new ImageIcon("./resources/sudoku.png")
    preferredSize_=(new Dimension(200, 160))
  }

  val flowPanel = new FlowPanel {
    contents += label
  }

  val buttonsFlowPanel = new FlowPanel {
    contents += playButton
    contents += Swing.VStrut(5)
    contents += solveButton
  }

  val labelFlowPanel = new FlowPanel {
    contents += new Label("Would you like to try your sudoku solving skills,")
    contents += new Label("or do you want your sudoku solved for you?")
  }

  contents = new BoxPanel(Orientation.Vertical) {
    contents += flowPanel
    contents += labelFlowPanel
    contents += buttonsFlowPanel
  }

  listenTo(playButton)
  listenTo(solveButton)

  reactions += {
    case ButtonClicked(`playButton`) => {
      val ui = new PlaySudokuWindow
      ui.visible = true
      close()
    }
    case ButtonClicked(`solveButton`) => {
      val ui = new InsertSudokuWindow
      ui.visible = true
      close()
    }
  }
}
