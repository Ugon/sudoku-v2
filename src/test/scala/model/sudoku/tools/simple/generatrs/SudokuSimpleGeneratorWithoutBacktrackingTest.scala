package model.sudoku.tools.simple.generatrs

import model.sudoku.tools.simple.generators.SudokuSimpleGeneratorWithoutBacktracking
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * @author Wojciech Pachuta.
 */
@RunWith(classOf[JUnitRunner])
class SudokuSimpleGeneratorWithoutBacktrackingTest extends AbstractSudokuGeneratorTest(SudokuSimpleGeneratorWithoutBacktracking)