package gui.sudoku

import java.awt.{Font, Dimension}

import _root_.model.sudoku.{Sudoku, Cell}

import scala.swing._
import scala.swing.event.KeyTyped

/**
 * Created by Piotr Wąchała
 */

trait GuiTrait {
  val windowSize = new Dimension(600, 400)
  val optionButtonsSize = new Dimension(90, 30)
  val sudokuTextFieldFont = new Font("SansSerif", Font.BOLD, 18)
  val sudokuTupleBorder = Swing.EmptyBorder(2, 2, 2, 2);
  val windowBorder = Swing.EmptyBorder(10)
  val errorColor = new java.awt.Color(250, 155, 150)
  val startValueColor = new java.awt.Color(95, 85, 160)
  val startColor = new java.awt.Color(200, 200, 200)
  val hintColor = new java.awt.Color(100,225,90)
  val standardColor = new java.awt.Color(255,255,255)
  val textFieldSize = new Dimension(40, 30)
  var sudoku : Sudoku

  val textFieldsArray = Array.ofDim[TextField](9, 9)

  def highlightCell(t: Int, c: Int, color: Color) = {
    textFieldsArray(t)(c).background = color
  }

  def setTextColor(t: Int, c: Int, color: Color) = {
    textFieldsArray(t)(c).foreground = color
  }

  def highlightWholeTuple(t: Int, color: Color) = {
    for(i <- (0 to 8)) textFieldsArray(t)(i).background = color
  }

  def distinguishStartValue(t: Int, c: Int)
  {
    highlightCell(t, c, startColor)
    setTextColor(t, c, startValueColor)
    textFieldsArray(t)(c).editable = false
  }

  def setStandardColorIfNeeded(r: Int, c: Int) = {
    if (textFieldsArray(r)(c).background != standardColor)
      textFieldsArray(r)(c).background = standardColor
  }

  def setFieldLabelWithProprietyCheck(row : Int, col: Int, e: KeyTyped, sudokuForm : Sudoku) ={
    e.char.isDigit match{
      case true => {
        sudokuForm.setLabel(Cell(row,col),e.char.asDigit) match {
          case true => highlightCell(row, col, errorColor)
          case false => {}
        }
      }
      case false => {
        e.consume
        sudokuForm.eraseLabel(Cell(row, col))
      }
    }
  }

  def initSudokuField() ={
    new GridPanel(3, 3) {
      for (j <- (0 to 8)) {
        contents += new GridPanel(3, 3){
          var starti = 3*(j%3)
          var startj = 0;

          if(j<3) startj = 0;
          else if(j<6) startj = 3;
          else startj = 6;

          for( k <- (0 to 2))
            for( l <- (0 to 2)){
              var newX = startj + k;
              var newY = starti + l;

              textFieldsArray(newX)(newY) = new TextField {
                text = ""
                columns = 5
                font = sudokuTextFieldFont
                horizontalAlignment = Alignment.Center
                listenTo(keys)
                reactions += {
                  case e: KeyTyped => {
                    if (!e.char.isDigit)
                      e.consume
                    setStandardColorIfNeeded(newX, newY)
                    setFieldLabelWithProprietyCheck(newX, newY, e, sudoku)
                  }
                }
              }

              contents += textFieldsArray(newX)(newY)
              sudoku.getLabel(Cell(newX, newY)) match{
                case None => textFieldsArray(newX)(newY).text = ""
                case Some(s) => {textFieldsArray(newX)(newY).text = s.toString;distinguishStartValue(newX,newY)}
              }
            }
          border = sudokuTupleBorder
        }
      }
    }
  }
}
