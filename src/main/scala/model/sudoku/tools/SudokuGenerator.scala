package model.sudoku.tools

import model.sudoku.Sudoku

/**
 * @author Wojciech Pachuta.
 */
trait SudokuGenerator {

  def generate(): Sudoku

}
