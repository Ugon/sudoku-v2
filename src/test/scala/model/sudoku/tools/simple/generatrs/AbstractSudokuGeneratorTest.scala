package model.sudoku.tools.simple.generatrs

import model.sudoku.{Cell, Sudoku}
import model.sudoku.tools.{SudokuGenerator}
import org.scalatest.FunSuite

/**
 * @author Wojciech Pachuta.
 */
abstract class AbstractSudokuGeneratorTest(val sudokuGenerator: SudokuGenerator) extends FunSuite{

  test("should generator generate not full grid") {
    //given

    //when
    val result = sudokuGenerator.generate()
    //then
    assert(result.filledCells.size < 40)
  }

}
